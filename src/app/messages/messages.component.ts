import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessageModel } from '../shared/message.model';
import { MessageService } from '../shared/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messagesArray: MessageModel[] = [];
  messagesArrayDate: string[] = [];
  messagesArraySubscription!: Subscription;
  fetchingLoadingSubscription!: Subscription;
  subscription!: Subscription;
  loading!: boolean;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.fetchingLoadingSubscription = this.messageService.fetchingMessagesArrayLoading.subscribe(loading => {
      this.loading = loading;
    })
    this.messagesArraySubscription = this.messageService.messagesArrayChange.subscribe( messagesArray => {
      this.messagesArray = messagesArray.reverse();
      this.messagesArrayDate = this.messageService.getDateTime(this.messagesArray);
    })
    const observable = this.messageService.start();
    this.subscription = observable.subscribe();
  }

  ngOnDestroy() {
    this.messagesArraySubscription.unsubscribe();
    this.fetchingLoadingSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }
}
