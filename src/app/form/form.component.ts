import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessageService } from '../shared/message.service';
import { NgForm } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy{
  @ViewChild('form') messageForm!: NgForm;
  addLoadingSubscription!: Subscription;
  loading = false;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.addLoadingSubscription = this.messageService.addMessageLoading.subscribe(loading => {
      this.loading = loading;
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.messageForm.form.setValue(value);
    })
  }

  onSendMessage() {
    const body = new HttpParams()
      .set('author', this.messageForm.value.author)
      .set('message', this.messageForm.value.message);
    this.messageService.addMessage(body);
    this.setFormValue({
      author: '',
      message: '',
    });
  }

  ngOnDestroy() {
    this.addLoadingSubscription.unsubscribe();
  }
}
