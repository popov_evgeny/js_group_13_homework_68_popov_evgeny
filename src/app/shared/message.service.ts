import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MessageModel } from './message.model';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable()

export class MessageService {
  private messagesArray: MessageModel[] | null = null;
  messagesArrayChange = new Subject<MessageModel[]>();
  fetchingMessagesArrayLoading = new Subject<boolean>();
  addMessageLoading = new Subject<boolean>();
  date = '';
  interval!: number;

  constructor(private http: HttpClient) {}

  start() {
    this.fetchingMessagesArrayLoading.next(true);
   return  new Observable(subscriber => {
     const interval = setInterval(() => {
       this.http.get<{ [id: string]: MessageModel }>(`http://146.185.154.90:8000/messages?datetime=${this.date}`).pipe(map(result => {
         if (result === null) {
           return [];
         }
         return Object.keys(result).map(id => {
           const message = result[id];
           return new MessageModel(message.datetime, message.author, message.message, message.id);
         })
       })).subscribe(messages => {
         if ( this.date === '') {
           this.date = messages[messages.length - 1].datetime;
           this.messagesArray = messages;
           this.messagesArrayChange.next(this.messagesArray.slice());
         } else {
           if (this.messagesArray !== null && messages.length !== 0){
             this.date = messages[messages.length - 1].datetime;
             this.messagesArray = this.messagesArray.concat(messages);
             this.messagesArrayChange.next(this.messagesArray.slice());
           }
         }
         this.fetchingMessagesArrayLoading.next(false);
       }, () => {
         this.fetchingMessagesArrayLoading.next(false);
       });
     }, 1500);
     return{
       unsubscribe() {
       clearInterval(interval);
     }};
   });
  }

  getDateTime(messagesArray: MessageModel[]) {
    return messagesArray.map( message => {
      return 'Date: ' + new Date(message.datetime).toLocaleDateString() + ' / Time: ' + new Date(message.datetime).toLocaleTimeString();
    });
  }

  addMessage(body: HttpParams) {
    this.addMessageLoading.next(true);
    this.http.post(`http://146.185.154.90:8000/messages`, body).subscribe(() => {
      this.addMessageLoading.next(false);
    }, () => {
      this.addMessageLoading.next(false);
    });
  }
}
