export class MessageModel {
  constructor(
    public datetime: string,
    public author: string,
    public message: string,
    public id: string
  ) {}
}
